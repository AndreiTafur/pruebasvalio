	'use strict'
//creacion del registro llamando al servicio rest
window.addEventListener('load',()=>{
	var form = document.querySelector("#form");
	console.log(form);
	form.addEventListener('submit',()=>{
		console.log('evento');
		let name = document.querySelector('#name').value;
		let lastName = document.querySelector('#lastName').value;
		let proceso = document.querySelector('#process').value;
		let data = {
			name:name,
			lastName:lastName,
			process:proceso
		}
		fetch('http://localhost:8080/users',{
			method:'POST',
			body:JSON.stringify(data),
			headers:{
				'Content-Type': 'application/json'
			}
		})	
		.then(function(response) {
			if(response.ok) {
				return response.text()
				location.reload(true);
			} else {
				throw "Error en la llamada Ajax";
			}
		});
	});
	
});
