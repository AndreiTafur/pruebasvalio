package com.project.backend.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import com.project.backend.model.User;
import com.project.backend.repository.IuserRepo;

@CrossOrigin
@org.springframework.web.bind.annotation.RestController
@RequestMapping({ "/users" })
public class RestController {

	@Autowired
	private IuserRepo userRepo;

	@GetMapping
	public List<User> listUser() {
		return userRepo.findAll();
	}

	@PostMapping
	public void createUser(@RequestBody User user) {
		userRepo.save(user);
	}

	@PutMapping("/user/{id}")
	public ResponseEntity<Object> modify(@PathVariable("id") int id, @RequestBody User user) {
		Optional<User> use = userRepo.findById(id);
		if (!use.isPresent())
			return ResponseEntity.badRequest().build();
		user.setId(id);
		userRepo.save(user);
		return ResponseEntity.noContent().build();
	}

}
